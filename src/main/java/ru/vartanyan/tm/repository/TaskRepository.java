package ru.vartanyan.tm.repository;

import ru.vartanyan.tm.api.repository.ITaskRepository;
import ru.vartanyan.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllByProjectId(final String projectId,
                                         final String userId) {
        final List<Task> listOfTask = new ArrayList<>();
        for (Task task: entities){
            if (task.getProjectId().equals(projectId) && task.getUserId().equals(userId)) listOfTask.add(task);
        }
        return listOfTask;
    }

    @Override
    public List<Task> removeAllByProjectId(final String projectId,
                                           final String userId) {
        for (Task task: entities) {
            if (task.getProjectId().equals(projectId) && task.getUserId().equals(userId)) task.setProjectId(null);
        }
        return entities;
    }

    @Override
    public Task bindTaskByProjectId(final String projectId,
                                    final String taskId,
                                    final String userId) {
        final Task task = findById(taskId, userId);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskFromProject(final String projectId,
                                      final String taskId,
                                      final String userID) {
        final Task task = findById(taskId, userID);
        task.setProjectId(null);
        return task;
    }

}
