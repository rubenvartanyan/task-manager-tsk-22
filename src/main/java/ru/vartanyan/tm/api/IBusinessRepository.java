package ru.vartanyan.tm.api;

import ru.vartanyan.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;

public interface IBusinessRepository<E extends AbstractBusinessEntity> extends IRepository<E> {

    void clear(String userId);

    List<E> findAll(final String userId);

    E findById(final String id, final String userId);

    void removeById(final String id, final String userId);

    E findOneByIndex(Integer index,
                     String userId) throws Exception;

    E findOneByName(String name,
                    String userId) throws Exception;

    void removeOneByIndex(Integer index,
                          String userId) throws Exception;

    void removeOneByName(String name,
                         String userId) throws Exception;

    List<E> findAll(Comparator<E> comparator,
                    String userId);

    void showEntity(E entity);

}
