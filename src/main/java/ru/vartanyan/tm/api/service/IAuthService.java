package ru.vartanyan.tm.api.service;

import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.model.User;

public interface IAuthService {

    User getUser() throws Exception;

    String getUserId() throws Exception;

    boolean isNotAuth();

    void logout();

    void login(final String login, final String password) throws Exception;

    void registry(final String login,
                         final String password,
                         final String email) throws Exception;

    void checkRoles(final Role... roles) throws Exception;

}
