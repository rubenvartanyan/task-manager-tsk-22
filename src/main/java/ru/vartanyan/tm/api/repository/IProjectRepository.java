package ru.vartanyan.tm.api.repository;

import ru.vartanyan.tm.api.IBusinessRepository;
import ru.vartanyan.tm.api.IRepository;
import ru.vartanyan.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IBusinessRepository<Project> {

    //List<Project> findAll(Comparator<Project> comparator, final String userId);

    //Project findOneByIndex(Integer index, final String userId);

    //Project findOneByName(String name, final String userId);

    //void removeOneByIndex(Integer index, final String userId);

    //void removeOneByName(String name, final String userId);

}
