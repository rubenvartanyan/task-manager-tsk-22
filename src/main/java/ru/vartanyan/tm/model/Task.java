package ru.vartanyan.tm.model;

import ru.vartanyan.tm.api.entity.IWBS;
import ru.vartanyan.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

public class Task extends AbstractBusinessEntity implements IWBS {

    public Task() {
    }

    private String projectId;

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }
}
