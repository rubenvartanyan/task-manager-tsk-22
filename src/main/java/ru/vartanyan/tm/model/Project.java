package ru.vartanyan.tm.model;

import ru.vartanyan.tm.api.entity.IWBS;
import ru.vartanyan.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

public class Project extends AbstractBusinessEntity implements IWBS {

    public Project() {
    }

    public Project(final String name, final String description) {
        this.name = name;
        this.description = description;
    }

}
