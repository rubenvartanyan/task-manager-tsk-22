package ru.vartanyan.tm.command.task;

import ru.vartanyan.tm.command.AbstractTaskCommand;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.model.Task;
import ru.vartanyan.tm.util.TerminalUtil;

import java.util.Arrays;

public class TaskUpdateStatusByIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-update-status-by-id";
    }

    @Override
    public String description() {
        return "Update task status by Id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE TASK STATUS]");
        if (serviceLocator.getAuthService().isNotAuth()) throw new NotLoggedInException();
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        System.out.println("[ENTER STATUS]");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Task task = serviceLocator.getTaskService().updateEntityStatusById(id, status, userId);
        System.out.println("[TASK STATUS UPDATED]");
    }

}
