package ru.vartanyan.tm.command.project;

import ru.vartanyan.tm.command.AbstractProjectCommand;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.util.TerminalUtil;

public class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-remove-by-index";
    }

    @Override
    public String description() {
        return "Remove project by index";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE PROJECT");
        if (serviceLocator.getAuthService().isNotAuth()) throw new NotLoggedInException();
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER INDEX");
        final Integer index = TerminalUtil.nextNumber() - 1;
        serviceLocator.getProjectService().removeOneByIndex(index, userId);
        System.out.println("[PROJECT REMOVED]");
    }

}
