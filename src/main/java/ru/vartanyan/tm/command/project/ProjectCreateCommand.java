package ru.vartanyan.tm.command.project;

import ru.vartanyan.tm.command.AbstractProjectCommand;
import ru.vartanyan.tm.exception.empty.EmptyDescriptionException;
import ru.vartanyan.tm.exception.empty.EmptyNameException;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.util.TerminalUtil;

public class ProjectCreateCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-create";
    }

    @Override
    public String description() {
        return "Create project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT CREATE]");
        if (serviceLocator.getAuthService().isNotAuth()) throw new NotLoggedInException();
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("ENTER MAME:");
        final String name = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final Project project = serviceLocator.getProjectService().add(name, description, userId);
        project.setUserId(userId);
        System.out.println("[PROJECT CREATED]");
    }

}
