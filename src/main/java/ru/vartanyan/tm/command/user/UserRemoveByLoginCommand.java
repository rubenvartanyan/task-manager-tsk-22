package ru.vartanyan.tm.command.user;

import ru.vartanyan.tm.command.AbstractUserCommand;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.model.User;
import ru.vartanyan.tm.util.TerminalUtil;

public class UserRemoveByLoginCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "user-remove-by-login";
    }

    @Override
    public String description() {
        return "Remove user by login";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE USER BY LOGIN]");
        System.out.println("[ENTER LOGIN]");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().removeByLogin(login);
        System.out.println("[USER REMOVED]");
    }

    @Override
    public Role[] roles() {
        return new Role[] {
                Role.ADMIN
        };
    }

}
