package ru.vartanyan.tm.command.user;

import ru.vartanyan.tm.command.AbstractUserCommand;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.model.Task;
import ru.vartanyan.tm.model.User;
import ru.vartanyan.tm.util.TerminalUtil;

public class UserRemoveByIdCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "user-remove-by-id";
    }

    @Override
    public String description() {
        return "Remove user by Id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("REMOVE USER BY ID");
        System.out.println("[ENTER ID]");
        final String userId = TerminalUtil.nextLine();
        serviceLocator.getUserService().removeById(userId);
        System.out.println("[USER REMOVED]");
    }

    @Override
    public Role[] roles() {
        return new Role[] {
                Role.ADMIN
        };
    }

}
