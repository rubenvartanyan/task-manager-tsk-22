package ru.vartanyan.tm;

import ru.vartanyan.tm.bootstrap.Bootstrap;

public class Application {

    public static void main(String[] args) throws Throwable {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
