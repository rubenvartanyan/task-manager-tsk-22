package ru.vartanyan.tm.service;

import ru.vartanyan.tm.api.IRepository;
import ru.vartanyan.tm.api.IService;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.exception.empty.EmptyDescriptionException;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.exception.empty.EmptyNameException;
import ru.vartanyan.tm.exception.incorrect.IncorrectIndexException;
import ru.vartanyan.tm.exception.system.NullTaskException;
import ru.vartanyan.tm.model.AbstractEntity;
import ru.vartanyan.tm.model.Task;

import java.util.Date;

public class AbstractService<E extends AbstractEntity> implements IService<E> {

    private final IRepository<E> repository;

    public AbstractService(IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public E add(E entity) {
        if (entity == null) return null;
        return repository.add(entity);
    }

    @Override
    public void remove(final E entity) {
        if (entity == null) return;
        repository.remove(entity);
    }

    @Override
    public E findById(final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id);
    }

    @Override
    public void removeById(final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        repository.removeById(id);
    }

}
