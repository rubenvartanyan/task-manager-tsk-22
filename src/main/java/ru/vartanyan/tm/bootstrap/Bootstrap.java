package ru.vartanyan.tm.bootstrap;

import ru.vartanyan.tm.api.IRepository;
import ru.vartanyan.tm.api.repository.ICommandRepository;
import ru.vartanyan.tm.api.repository.IProjectRepository;
import ru.vartanyan.tm.api.repository.ITaskRepository;
import ru.vartanyan.tm.api.repository.IUserRepository;
import ru.vartanyan.tm.api.service.*;
import ru.vartanyan.tm.command.AbstractCommand;
import ru.vartanyan.tm.command.auth.AuthLoginCommand;
import ru.vartanyan.tm.command.auth.AuthLogoutCommand;
import ru.vartanyan.tm.command.auth.AuthRegistryCommand;
import ru.vartanyan.tm.command.auth.AuthViewProfileCommand;
import ru.vartanyan.tm.command.project.*;
import ru.vartanyan.tm.command.system.AboutCommand;
import ru.vartanyan.tm.command.system.ExitCommand;
import ru.vartanyan.tm.command.system.InfoCommand;
import ru.vartanyan.tm.command.system.VersionCommand;
import ru.vartanyan.tm.command.task.*;
import ru.vartanyan.tm.command.user.*;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.repository.*;
import ru.vartanyan.tm.service.*;
import ru.vartanyan.tm.util.TerminalUtil;

public final class Bootstrap implements ServiceLocator{

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();
    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    private final IUserRepository userRepository = new UserRepository();
    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ILoggerService loggerService = new LoggerService();

    /*void initData() throws Exception {
        projectService.add("BBC", "-", "1111").setStatus(Status.NOT_STARTED);
        projectService.add("ABC", "-", "2222").setStatus(Status.COMPLETE);
        projectService.add("AAC", "-", "3333").setStatus(Status.IN_PROGRESS);
        taskService.add("BBB", "-").setStatus(Status.NOT_STARTED);
        taskService.add("FFF", "-").setStatus(Status.IN_PROGRESS);
        taskService.add("AAA", "-").setStatus(Status.COMPLETE);
    }*/

    void initUser() throws Exception {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    {
        registry(new TaskBindByProjectIdCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskFindAllByProjectIdCommand());
        registry(new TaskFinishByIdCommand());
        registry(new TaskFinishByNameCommand());
        registry(new TaskFinishByIndexCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowListCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByNameCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByNameCommand());
        registry(new TaskUnbindByProjectIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateStatusByIdCommand());
        registry(new TaskUpdateStatusByNameCommand());
        registry(new TaskUpdateStatusByNameCommand());

        registry(new ProjectAndTaskRemoveByIdCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectFinishByIdCommand());
        registry(new ProjectFinishByNameCommand());
        registry(new ProjectFinishByIndexCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByNameCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByNameCommand());
        registry(new ProjectUpdateStatusByIdCommand());
        registry(new ProjectUpdateStatusByIndexCommand());
        registry(new ProjectUpdateStatusByNameCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new InfoCommand());
        registry(new AboutCommand());
        registry(new ExitCommand());
        registry(new VersionCommand());

        registry(new AuthLoginCommand());
        registry(new AuthLogoutCommand());
        registry(new AuthRegistryCommand());
        registry(new AuthViewProfileCommand());

        registry(new UserChangePasswordCommand());
        registry(new UserRemoveByIdCommand());
        registry(new UserRemoveByLoginCommand());
        registry(new UserUpdateProfileCommand());

        registry(new UserLockByIdCommand());
        registry(new UserLockByLoginCommand());
        registry(new UserUnlockByIdCommand());
        registry(new UserUnlockByLoginCommand());
    }

    public void run(final String... args) throws Throwable {
        loggerService.debug("TEST!");
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        //initData();
        initUser();
        while (true){
            System.out.println();
            System.out.println("ENTER COMMAND:");
            final String command = TerminalUtil.nextLine();
            loggerService.command(command);
            try {
                parseCommand(command);
                System.out.println("[OK]");
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    public boolean parseArgs(String[] args) throws Exception {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private void parseArg(final String arg) throws Exception {
        if (arg == null || arg.isEmpty()) return;
        final AbstractCommand command = commandService.getCommandByName(arg);
        if (command == null) showIncorrectArg();
        else command.execute();
    }

    public void showIncorrectArg() {
        System.out.println("Error! Argument not found...");
    }

    public void parseCommand(final String cmd) throws Exception {
        if (cmd == null || cmd.isEmpty()) return;
        final AbstractCommand command = commandService.getCommandByName(cmd);
        if (command == null) {
            showIncorrectCommand();
            System.out.println("error is here");
            return;
        }
        final Role[] roles = command.roles();
        authService.checkRoles(roles);
        command.execute();
    }

    private void registry(final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public static void showIncorrectCommand() {
        System.out.println("Error! Command not found...");
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

}
